/**
 * The add seting component
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Settings } from '/imports/api/settings/settings.js';
import { Meteor } from 'meteor/meteor';
import './addsetting.html';

/**
 * Autoform Hooks
 */

AutoForm.hooks({
    addSettingForm: {
        after:
        {
            insert: function (err, result)
            {
                if(!err) {
                    AddSetting.close();
                    NotifyHelpers.success('Insert success.', 'The setting "' + this.insertDoc.key + '" has been added.');
                } else {
                    AdminModal.showError("Insert error", err.message);
                }
            }
        }
    }
});

/**
 * The Helpers
 */

AddSetting = {
    close: function() {
        $('#addSetting').modal('hide');
        AddSetting.resetForm();
    },
    resetForm: function() {
        AutoForm.resetForm('addSettingForm');
    },
    show: function() {
        $('#addSetting').modal('show');
    }
};

/**
 * The Template
 */

Template.addsetting.onRendered(() => {
    $('#addSetting').on('hidden.bs.modal', function () {
        AddSetting.resetForm();
    });
});

Template.addsetting.onCreated(() => {
});

Template.addsetting.helpers({
});

Template.addsetting.events({
});
