/**
 * The change password component
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Users } from '/imports/api/users/users.js';
import { Meteor } from 'meteor/meteor';
import './changepassword.html';

/**
 * Autoform Hooks
 */

AutoForm.hooks({
    changePasswordForm: {
        before:
        {
            update: function()
            {
                // get our modal
                var modal = $('#changePassword');

                // get the new and check password
                var newpassword = modal.find('.txtPassword').val();;
                var checkpassword = modal.find('.txtCheckPassword').val();

                // validate
                if(newpassword == checkpassword) {
                    return { $set: { password: newpassword } };
                } else {
                    AdminModal.showError('Match error', 'The passwords do not match. Try again.');
                    this.resetForm();
                }
            }
        },
        after:
        {
            update: function (err, result)
            {
                if(!err) {
                    var user = Session.get('user');
                    ChangePassword.close();
                    NotifyHelpers.success('User password changed.', 'The password for user "' + user.username + '" has been changed.');
                    this.resetForm();
                } else {
                    AdminModal.showError("Update error", err.message);
                }
            }
        }
    }
});

/**
 * The Helpers
 */

ChangePassword = {
    close: function() {
        $('#changePassword').modal('hide');
        ChangePassword.resetForm();
    },
    resetForm: function() {
        AutoForm.resetForm('changePasswordForm');
    },
    show: function(id) {
        var userExists = typeof Users.findOne({ _id: id }) !== "undefined";
        if(userExists) {
            Session.set('user', Users.findOne({ _id: id }));
            $('#changePassword').modal('show');
        } else {
            NotifyHelpers.error('User error.', 'The user does not exist.');
        }
    }
};

/**
 * The Template
 */

Template.changepassword.onRendered(() => {
    $('#changePassword').on('hidden.bs.modal', function () {
        ChangePassword.resetForm();
    });
});

Template.changepassword.onCreated(() => {
});

Template.changepassword.helpers({
    user() {
        return Session.get('user');
    }
});

Template.changepassword.events({
});