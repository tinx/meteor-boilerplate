/**
 * The edit users component
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Users } from '/imports/api/users/users.js';
import { Meteor } from 'meteor/meteor';
import './edituser.html';

/**
 * Autoform Hooks
 */

AutoForm.hooks({
    editUserForm: {
        after:
        {
            update: function (err, result)
            {
                if(!err) {
                    var user = Session.get('user');
                    EditUser.close();
                    NotifyHelpers.success('User updated.', 'The user "' + user.username + '" has been updated.');
                } else {
                    AdminModal.showError("Update error", err.message);
                }
            }
        }
    }
});

/**
 * The Helpers
 */

EditUser = {
    close: function() {
        $('#editUser').modal('hide');
        EditUser.resetForm();
    },
    resetForm: function() {
        AutoForm.resetForm('editUserForm');
    },
    show: function(id) {
        var userExists = typeof Users.findOne({ _id: id }) !== "undefined";
        if(userExists) {
            Session.set('user', Users.findOne({ _id: id }));
            $('#editUser').modal('show');
        } else {
            NotifyHelpers.error('User error.', 'The user does not exist.');
        }
    }
};

/**
 * The Template
 */

Template.edituser.onRendered(() => {
    $('#editUser').on('hidden.bs.modal', function () {
        EditUser.resetForm();
    });
});

Template.edituser.onCreated(() => {
});

Template.edituser.helpers({
    user() {
        return Session.get('user');
    }
});

Template.edituser.events({
});
