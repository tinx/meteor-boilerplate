/**
 * The add user component
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Users } from '/imports/api/users/users.js';
import { Meteor } from 'meteor/meteor';
import './adduser.html';

/**
 * Autoform Hooks
 */

AutoForm.hooks({
    addUserForm: {
        after:
        {
            insert: function (err, result)
            {
                if(!err) {
                    AddUser.close();
                    NotifyHelpers.success('Insert success.', 'The user "' + this.insertDoc.username + '" has been added.');
                } else {
                    AdminModal.showError("Insert error", err.message);
                }
            }
        }
    }
});

/**
 * The Helpers
 */

AddUser = {
    close: function() {
        $('#addUser').modal('hide');
        AddUser.resetForm();
    },
    resetForm: function() {
        AutoForm.resetForm('addUserForm');
    },
    show: function() {
        $('#addUser').modal('show');
    }
};

/**
 * The Template
 */

Template.adduser.onRendered(() => {
    $('#addUser').on('hidden.bs.modal', function () {
        AddUser.resetForm();
    });
});

Template.adduser.onCreated(() => {
});

Template.adduser.helpers({
});

Template.adduser.events({
});