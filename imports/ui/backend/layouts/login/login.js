/**
 * Import the login page layout
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Session } from 'meteor/session';
import './login.html';

LoginTemplateHelper = {
    login: function()
    {
        // get username and password
        var username = $('.txtUsername').val();
        var password = $('.txtPassword').val();
        var valid = true;
        var errormessages = [];

        // validate
        if(username == "") {
            errormessages.push('Please provide a username to login.');
            valid = false;
        }
        if(password == "") {
            errormessages.push('Please provide a password to login.');
            valid = false;
        }

        // this is not valid
        if(!valid) {
            var errormessage = "";
            errormessages.forEach(message => errormessage += (message + "<br/>"));
            NotifyHelpers.error('Login failed.', errormessage);
        }

        else
        {
            Meteor.call('users.loginByUsername', username, password, function(err, userId) {
                if(userId) {
                    // set local storage
                    localStorage.setItem('backendUserId', userId);
                    // give a notification
                    NotifyHelpers.info('Login succeeded.', "Welcome to your application!");
                    // go to dashboard
                    FlowRouter.go('Backend.dashboard');
                }
                else {
                    NotifyHelpers.error('Login failed.', "You cannot login with the given credentials.");
                }
            });
        }
    }
}

// LOGIC

Template.Backend_login.onRendered(function() {
});

Template.Backend_login.helpers({
});

Template.Backend_login.rendered = function() {
};

// events
Template.Backend_login.events({
    'click .btnLogin': function(e) {
        LoginTemplateHelper.login();
    },
    "keyup .txtLogin": function(ev)
    {
        if(ev.which === 13) {LoginTemplateHelper.login();}
    }
});