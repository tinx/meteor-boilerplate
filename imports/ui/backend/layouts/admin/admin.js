/**
 * Import the Backend body
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Session } from 'meteor/session'
import { Modules } from '/imports/api/modules/modules.js';
import './admin.html';

// SUBSCRIPTIONS

Meteor.subscribe('modules.all');

// LOGIC

Template.Backend_admin.onRendered(function() {
    Session.set("title", this.data.data().title);
});

Template.Backend_admin.helpers({
    title() {
        return Session.get("title");
    },
    links() {
        var links = [
            {
                label: "Dashboard",
                path: "/admin",
                icon: "circle-o",
                hasChildren: false
            },
            {
                label: "Configuration",
                icon: "cog",
                hasChildren: true,
                children: [
                    {
                        label: "Users",
                        path: "/admin/cms/users"
                    },
                    {
                        label: "Settings",
                        path: "/admin/cms/settings"
                    }
                ]
            }
        ]

        // first validate if we need to insert current modules
        var m = Modules.find().map((m) => {
            return {
                label: m.label,
                path: m.path
            }
        });

        if(typeof m !== "undefined" && m.length > 0)
        {
            links.splice(1, 0, {
                label: "Modules",
                icon: "wrench",
                hasChildren: true,
                children: m
            });
        }

        //return the links
        return links;
    }
});

Template.Backend_admin.rendered = function() {
    $('.sidebar-toggle').each(() =>
    {
        var group = $(this);
        $(this).find(".btn").click(function(e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });
    });

    $('.sidebar-toggle').click((e) =>
    {
        e.preventDefault();

        // enable sidebar push menu
        $("body").toggleClass('sidebar-collapse');
        $("body").toggleClass('sidebar-open');
    });

    $(".content-wrapper").click(() =>
    {
        // enable hide menu when clicking on the content-wrapper on small screens
        if ($(window).width() <= 767 && $("body").hasClass("sidebar-open"))
        {
            $("body").removeClass('sidebar-open');
        }
    });

    $("li a", $('.sidebar')).click((e) =>
    {
        // get the clicked link and the next element
        var $this = $(this);
        var checkElement = $this.next();

        // check if the next element is a menu and is visible
        if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')))
        {
            // close the menu
            checkElement.slideUp('normal', function() {
                checkElement.removeClass('menu-open');
            });
            checkElement.parent("li").removeClass("active");
        }

        // if the menu is not visible
        else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible')))
        {
            //Get the parent menu
            var parent = $this.parents('ul').first();
            //Close all open menus within the parent
            var ul = parent.find('ul:visible').slideUp('normal');
            //Remove the menu-open class from the parent
            ul.removeClass('menu-open');
            //Get the parent li
            var parent_li = $this.parent("li");

            //Open the target menu and add the menu-open class
            checkElement.slideDown('normal', function() {
                //Add the class active to the parent li
                checkElement.addClass('menu-open');
                parent.find('li.active').removeClass('active');
                parent_li.addClass('active');
            });
        }

        //if this isn't a link, prevent the page from being redirected
        if (checkElement.is('.treeview-menu'))
        {
            e.preventDefault();
        }
    });
}

// events
Template.Backend_admin.events({
    'click .btnLogout': function(e)
    {
        AdminModal.showConfirmation("Logout", 'Do you really want to logout?', function() {
            var backendUserId = localStorage.getItem("backendUserId");
            Meteor.call('users.logout', backendUserId, function(e, r) {
                localStorage.removeItem('backendUserId');
                FlowRouter.go('Backend.login');
                NotifyHelpers.info('Succesfully logged out', 'Goodbye! See you next time.');
            });
        });
    }
});