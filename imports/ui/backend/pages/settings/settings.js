/**
 * The settings template
 *
 * @author Tim De Paepe <tim.depaepe@tinx.be>
 */

import { Meteor } from 'meteor/meteor';
import { Settings } from '/imports/api/settings/settings.js';
import '../../components/settings/addsetting/addsetting.js';
import './settings.html';

// on created
Template.settings.onRendered(function()
{
  Session.set("title", this.data.data().title);
});

// helpers
Template.settings.helpers({
  title() {
    return Session.get('title');
  },
  hasSettings() {
    return Settings.find().count() > 0;
  },
  settings() {
    return Settings.find({}, { sort: { key: 1 }});
  }
});

// events
Template.settings.events({
  'click .btnAdd': (ev) => {
    AddSetting.show();
  },
  'click .save': function(e)
  {
    // get the key
    var key = this.key;

    // get the new value
    var newValue = $('.' + key).val();

    // save the setting
    Meteor.call('settings.save', this.key, newValue, (err, re) => {
      if(err) { AdminModal.showError(err.error, err.reason )}
      else {
        NotifyHelpers.success('Succesfully saved', 'The setting "' + key + '" was succesfully saved.');
      }
    });
  },
  'click .delete': function(e)
  {
    var id = this._id;
    var key = this.key;
    AdminModal.showConfirmation("delete setting", 'Do you really want to delete setting "' + key + '"?', function() {
      Meteor.call('settings.remove', id, function(err, res) {
        if(err) { AdminModal.showError(err.error, err.reason )}
        else {
          NotifyHelpers.success('Succesfully removed', 'The setting "' + key + '" was succesfully removed.');
        }
      });
    })
  }
});
