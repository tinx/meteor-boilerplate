/**
 * The users template
 *
 * @author Tim De Paepe <tim.depaepe@tinx.be>
 */

import { Meteor } from 'meteor/meteor';
import { Users } from '/imports/api/users/users.js';
import '../../components/users/adduser/adduser.js';
import '../../components/users/edituser/edituser.js';
import '../../components/users/changepassword/changepassword.js';
import './users.html';

// on created
Template.users.onRendered(function() {
  Session.set("title", this.data.data().title);
});

// helpers
Template.users.helpers({
  title() {
    return Session.get('title');
  },
  hasUsers() {
    return Users.find().count() > 0;
  },
  users() {
    return Users.find({}, { sort: { username: 1 }});
  }
});

// events
Template.users.events({
  'click .btnAdd': (ev) => {
    AddUser.show();
  },

  'click .edit': function(e) {
    EditUser.show(this._id);
  },

  'click .change-password': function(e) {
    ChangePassword.show(this._id);
  },

  'click .delete': function(e)
  {
    var id = this._id;
    var username = this.username;
    AdminModal.showConfirmation("delete user", 'Do you really want to delete user "' + username + '"?', function() {
      Meteor.call('users.remove', id, function(err, res) {
        if(err) { AdminModal.showError(err.error, err.reason )}
        else {
          NotifyHelpers.success('Succesfully removed', 'The user "' + username + '" was succesfully removed.');
        }
      });
    });
  }

});
