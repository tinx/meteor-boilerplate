/**
 * The Home page template
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import './home.html';

import '../../components/info/info.js';
