/**
 * The default info component
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Settings } from '/imports/api/settings/settings.js';
import { Meteor } from 'meteor/meteor';
import './info.html';

Template.info.onCreated(() => {
    Meteor.subscribe('settings.all');
});

Template.info.onRendered(() => {
});

Template.info.helpers({
});

Template.info.events({
});
