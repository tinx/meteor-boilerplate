/**
 * All modules-related publications
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { Modules } from '../modules.js';

Meteor.publish('modules.all', function () {
  return Modules.find();
});