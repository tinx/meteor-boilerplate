/*
 * Methods related to Modules collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Modules } from './modules.js';

Meteor.methods({  
  'modules.save'(data)
  {
    // validate
    check(data.name, String);
    check(data.label, String);
    check(data.path, String);

    // get one of the modules, if one exists
    var obj = Modules.findOne({ _id: data._id });

    // validate and save
    if(typeof obj !== 'undefined') {
        Modules.update({ _id: obj._id }, { $set: data });
    } else {
        Modules.insert(data);
    }    
  },
  'modules.remove'(_id)
  {
    // validate
    check(_id, String);

    // validate and remove
    if(typeof _id != "undefined") {
        Modules.remove({ _id: _id });
    }
  }
});
