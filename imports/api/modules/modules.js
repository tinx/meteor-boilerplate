/**
 * The Modules collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

/**
 * Define Mongo Collection
 */
export const Modules = new Mongo.Collection("modules");

/**
 * Attach a Schema
 */

var Schemas = {};

// create simpleschema
Schemas.Module = new SimpleSchema({
    name: {
        type: String,
        optional: false
    },
    path: {
        type: String,
        optional: false
    },
    label: {
        type: String,
        optional: false
    },
    addedOn: {
        type:Date,
        optional: false,
        defaultValue: new Date()
    }
});

// attach schema
Modules.attachSchema(Schemas.Module);