/**
 * The Users collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/**
 * Define Mongo Collection
 */

export const Users = new Mongo.Collection("users");

/**
 * Attach a Schema
 */

var Schemas = {};

// create simpleschema
Schemas.User = new SimpleSchema({
  username:{
    type:String,
    optional: false
  },
  email: {
    type:String,
    optional: false
  },
  isGod: {
    type: Boolean,
    optional: false,
    defaultValue: false
  },
  password: {
    type:String,
    optional: false
  },
  loggedIn: {
    type:Boolean,
    optional:false,
    defaultValue: false
  },
  lastLogin: {
    type:Date,
    optional: true
  },
  addedOn: {
    type:Date,
    optional: false,
    defaultValue: new Date()
  }
}, { tracker: Tracker });

// attach schema
Users.attachSchema(Schemas.User);

// validations
Users.allow({
  insert: function () { return true; },
  update: function () { return true; },
  remove: function () { return true; }
});