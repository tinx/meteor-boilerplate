/*
 * Methods related to Users collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Users } from './users.js';
import { userInfo } from 'os';

Meteor.methods({
  'users.checkloggedin'(id)
  {
    // validate
    check(id, String);

    // validate existing user
    var user = Users.findOne({ _id: id });

    // validate
    if(typeof user !== "undefined")
    {
      // get the time since last login
      var lastLogin = moment(user.lastLogin).format('x');
      var now = moment().format('x');
      var diff = Math.floor((now - lastLogin) / 1000);

      // when we are logged in, more than 12 hours
      if(user.loggedIn && diff >= 43200)
      {
        // set logged in as false
        Users.update({ _id: user.id }, { $set: { loggedIn: false } });
        return false;
      }

      else
      {
        return user.loggedIn;
      }
    }

     // anyway...
     return false;
  },
  'users.loginByUsername'(username, password)
  {
    // validate
    check(username, String);
    check(password, String);

    // validate existing user
    var user = Users.findOne({ username: username, password: password });

    // log in the user
    if(typeof user !== "undefined") {
      Users.update({ _id: user._id },  { $set: { loggedIn: true, lastLogin: new Date() } });
      return user._id;
    } else {
      return null;
    }
  },
  'users.logout'(id)
  {
    // validate
    check(id, String);

    // validate existing user
    var user = Users.findOne({ _id: id });

    // log in the user
    if(typeof user !== "undefined") {
      Users.update({ _id: user._id },  { $set: { loggedIn: false } });
    }
  },
  'users.remove'(_id)
  {
    // validate
    check(_id, String);

    if(typeof _id != "undefined") {
      Users.remove({ _id: _id });
    }
  }
});
