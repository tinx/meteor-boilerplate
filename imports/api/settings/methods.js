/*
 * Methods related to Settings collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Settings } from './settings.js';

Meteor.methods({  
  'settings.save'(key, value)
  {
    // validate
    check(key, String);
    check(value, String);
    
    if(typeof value !== 'undefined')
    {
      // define the visitor data
      var settingData = {
        key: key,
        value: value
      };

      // get setting (if it exists)
      var setting = Settings.find({ key: settingData.key }).fetch();

      if(typeof setting !== 'undefined' && setting.length > 0) {
        Settings.update({ _id: setting[0]._id }, { $set: settingData });
      } else {
        settingData.addedOn = new Date();
        Settings.insert(settingData);
      }
    }
  },
  'settings.remove'(_id)
  {
    // validate
    check(_id, String);

    if(typeof _id != "undefined") {
      Settings.remove({ _id: _id });
    }
  }
});
