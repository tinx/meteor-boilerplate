/**
 * All settings-related publications
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { Settings } from '../settings.js';

Meteor.publish('settings.all', function () {
  return Settings.find();
});
