/**
 * The Settings collection
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

/**
 * Define Mongo Collection
 */

export const Settings = new Mongo.Collection("settings");

/**
 * Attach a Schema
 */

var Schemas = {};

// create simpleschema
Schemas.Setting = new SimpleSchema({
  key:{
    type:String,
    optional: false
  },
  value: {
    type:String,
    optional: false
  },
  addedOn: {
    type:Date,
    optional: false,
    defaultValue: new Date()
  }
}, { tracker: Tracker });

// attach schema
Settings.attachSchema(Schemas.Setting);

// validations
Settings.allow({
  insert: function () { return true; },
  update: function () { return true; },
  remove: function () { return true; }
});