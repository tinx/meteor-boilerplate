/**
 * Define the routes for Backend client
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Settings } from '/imports/api/settings/settings.js';

// Import needed layouts
import '../../ui/backend/layouts/admin/admin.js';
import '../../ui/backend/layouts/login/login.js';

// Import needed pages
import '../../ui/backend/pages/dashboard/dashboard.js';
import '../../ui/backend/pages/settings/settings.js';
import '../../ui/backend/pages/users/users.js';
// ### IMPORT NEW PAGES REFERER ###

var backendRoutes = FlowRouter.group({
  name: 'Backend.admin', 
  prefix: '/admin',  
  triggersEnter: [enterBackend]
});

var loginRoutes = FlowRouter.group({
  name: 'Backend.login',
  prefix: '/admin',  
  triggersEnter: [enterLogin]
});

/**
 * Login
 */

loginRoutes.route('/login', {
  name: 'Backend.login',
  action() {
    DocHead.setTitle("Login // Backend");
    BlazeLayout.render('Backend_login');
  }
});

/**
 * Dashboard
 */

backendRoutes.route('/', {
  name: 'Backend.dashboard',
  action() {
    var data = { title: "Dashboard"}
    DocHead.setTitle(data.title + " // Backend");
    BlazeLayout.render('Backend_admin', { main: 'Backend_dashboard', data: data });
  }
});

/**
 * CMS - Settings
 */

backendRoutes.route('/cms/settings', {
  name: 'Backend.settings',
  subscriptions() {
    this.register('settings', Meteor.subscribe('settings.all'));
  },
  action() {
    var data = { title: "Settings"}
    DocHead.setTitle(data.title + " // Backend");
    BlazeLayout.render('Backend_admin', { main: 'settings',  data: data });
  }
});

/**
 * CMS - Users
 */

backendRoutes.route('/cms/users', {
  name: 'Backend.users',
  subscriptions() {
    this.register('users', Meteor.subscribe('users.all'));
  },
  action() {
    var data = { title: "Users"}
    DocHead.setTitle(data.title + " // Backend");
    BlazeLayout.render('Backend_admin', { main: 'users', data: { title: 'Users' } });
  }
});

// ### IMPORT NEW ROUTES REFERER ###

/**
 * METHODS
 */

function enterBackend(context)
{
  // get the backend user id
  var backendUserId = localStorage.getItem("backendUserId");

  // check if we have a backend user in local storage
  if(backendUserId) {
    // check logged in state
    Meteor.call('users.checkloggedin', backendUserId, function(err, loggedIn) 
    {
      if(loggedIn) {
        $('body').removeClass('hold-transition login-page');
        $('body').addClass('skin-blue fixed sidebar-mini backend-body');
      } else {
        localStorage.removeItem("backendUserId");
        FlowRouter.go('Backend.login');
      }
    });
  }

  else { FlowRouter.go('Backend.login'); }    
}

function enterLogin(context) 
{
  // get the backend user id
  var backendUserId = localStorage.getItem("backendUserId");
  
  // check if we have a backend user in local storage
  if(backendUserId) {
    // check logged in state
    Meteor.call('users.checkloggedin', backendUserId, function(err, loggedIn) 
    {
      if(loggedIn) {
        FlowRouter.go('Backend.dashboard');
      } else {        
        $('body').removeClass('skin-blue fixed');
        $('body').addClass('hold-transition login-page');
      }
    });
  }

  else
  {
    $('body').removeClass('skin-blue fixed');
    $('body').addClass('hold-transition login-page');
  }  
}
