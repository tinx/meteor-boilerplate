/**
 * The Client Helpers
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Settings } from '/imports/api/settings/settings.js';

// subscriptions

Meteor.subscribe('settings.all');

// helper class

Boilerplate = {

    debug: true,

    getSetting: function(key, defaultValue) {
        if(typeof Settings.findOne({ key: key }) !== 'undefined') {
            return Settings.find({ key: key }).fetch()[0]['value'];
        } else {
            if(typeof defaultValue !== 'undefined') return defaultValue;
            else return '';
        }
    },

    log: function(message, type) {
        if(ClientHelpers.debug) {
            var typeString = "INFO";
            if (type == "E") { typeString = "ERROR" }
            else if (type == "W") { typeString = "WARNING" }
            console.log("LOG - " + typeString + " - " + message);
        }
    }

};