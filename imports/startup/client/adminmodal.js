/**
 * The Admin Modals
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import swal from 'sweetalert2';

AdminModal = {
    showError: function(title, message) {
        swal(
        {
            title: title.ucfirst(),
            text: message,
            type: "error"
        });
    },
    showInfo: function(title, message) {
        swal(
        {
            title: title.ucfirst(),
            text: message,
            type: "info"
        });
    },
    showConfirmation: function(title, message, callback) {
        swal(
        {
            title: title.ucfirst(),
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                callback();
            }
        });
    }
};