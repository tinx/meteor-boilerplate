/**
 * Define the routes for Frontend client
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/frontend/layouts/body/body.js';

// Import needed pages
import '../../ui/frontend/pages/home/home.js';
// ### IMPORT NEW PAGES REFERER ###

var frontendRoutes = FlowRouter.group({
  prefix: '',
  name: 'Frontend.app',
  triggersEnter: [enterFrontend]
});

/**
 * Home
 */

frontendRoutes.route('/', {
  name: 'Frontend.home',
  action() {
    BlazeLayout.render('Frontend_body', { main: 'Frontend_home' });
  },
});

// ### IMPORT NEW ROUTES REFERER ###

/**
 * METHODS
 */

function enterFrontend(context) {
  $('body').addClass('frontend-body');
}