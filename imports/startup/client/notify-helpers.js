/**
 * The Notify Helpers
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

// helper class

NotifyHelpers = {
    
    hideTimer: 3,

    success: function(title, message) {
        var notificationId = Notifications.success(title, message);
        Meteor.setTimeout( function () {
            Notifications.remove({ _id: notificationId });
        }, NotifyHelpers.hideTimer * 1000 );
    },

    warn: function(title, message) {
        var notificationId = Notifications.warn(title, message);
        Meteor.setTimeout( function () {
            Notifications.remove({ _id: notificationId });
        }, NotifyHelpers.hideTimer * 1000 );
    },

    info: function(title, message) {
        var notificationId = Notifications.info(title, message);
        Meteor.setTimeout( function () {
            Notifications.remove({ _id: notificationId });
        }, NotifyHelpers.hideTimer * 1000 );
    },

    error: function(title, message) {
        var notificationId = Notifications.error(title, message);
        Meteor.setTimeout( function () {
            Notifications.remove({ _id: notificationId });
        }, NotifyHelpers.hideTimer * 1000 );
    }

}