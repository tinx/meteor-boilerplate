/**
 * Import client startup through a single index entry point
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Start the main thread
 */
import './main.js';

/**
 * Add template helpers
 */

import './boilerplate.js';
import './adminmodal.js';
import './notify-helpers.js';
import './spacebarhelpers.js';

/**
 * Do routing
 */

import './routes_shared.js';
import './routes_frontend.js';
import './routes_backend.js';