/**
 * The main client thread
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Settings } from '/imports/api/settings/settings.js';
import { Users } from '/imports/api/users/users.js';

/**
 * Add collections to window scope
 */

window.Settings = Settings;
window.Users = Users;