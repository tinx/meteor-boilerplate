/**
 * Define the shared routes for client
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/frontend/layouts/body/body.js';

// Import needed pages
import '../../ui/frontend/pages/not-found/not-found.js';

// Set up all routes in the app
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('Frontend_body', { main: 'Frontend_notFound' });
  },
};