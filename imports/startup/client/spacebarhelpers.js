/**
 * Extend the templates
 *
 * @author Tim De Paepe <tim.depaepe@tinx.be>
 */

import { Meteor } from 'meteor/meteor';

/**
 * Collections
 */

Template.registerHelper('setting', function(key) {
    return Boilerplate.getSetting(key);
});

/**
 * Dates
 */

Template.registerHelper('longDate', function(date) {
    return date.longDate();
});

/**
 * Array
 */

Template.registerHelper('isEmpty', function(array) {
    return array.isEmpty();
});

/**
 * String
 */

Template.registerHelper('ucfirst', function(string) {
    return string.ucfirst();
});