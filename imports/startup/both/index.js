/**
 * Import modules used by both client and server through a single index entry point
 * e.g. useraccounts configuration file.
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import './prototypes/prototypes.js';