/**
 * New prototypes for Date
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Makes a long date
 */

if(!Date.prototype.longDate) {
    Object.defineProperty(Date.prototype, "longDate", {
        enumerable: false,
        value: function() {
            return moment(this).format('DD/MM/YYYY -- HH:mm.ss');
        }
    });
}