/**
 * New prototypes for Array
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Join2
 */

if(!Array.prototype.join2) {
    Object.defineProperty(Array.prototype, "join2", {
        enumerable: false,
        value: function(all, last) {
            return this.slice(0, -1).join(all)+last+this.slice(-1);
        }
    });
}

/**
 * Checks if an array contains a certain value
 */

if(!Array.prototype.contains) {
    Object.defineProperty(Array.prototype, "contains", {
        enumerable: false,
        value: function(value) {
            return this.indexOf(value) > -1;
        }
    });
}

/**
 * Checks if an array is empty
 */

if(!Array.prototype.isEmpty) {
    Object.defineProperty(Array.prototype, "isEmpty", {
        enumerable: false,
        value: function() {
            if(typeof this == "undefined" || this == null) return true;
            else { return this.length == 0; }
        }
    });
}