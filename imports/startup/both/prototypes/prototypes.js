
/**
 * New prototypes for Javascript
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import './object.js';
import './string.js';
import './array.js';
import './date.js';