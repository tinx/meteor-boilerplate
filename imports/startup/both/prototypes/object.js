/**
 * New prototypes for Object
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Checks if the object is not empty => {}
 */

if(!Object.prototype.isEmpty) {
    Object.defineProperty(Object.prototype, "isEmpty", {
        enumerable: false,
        value: function() {
            for(var key in this) {
                if(this.hasOwnProperty(key))
                    return false;
            }
            return true;
        }
    });
}