/**
 * New prototypes for String
 *
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Makes the first letter of a string capital
 */

if(!String.prototype.ucfirst) {
    Object.defineProperty(String.prototype, "ucfirst", {
        enumerable: false,
        value: function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
    });
}

/**
 * Makes a string URL friendly
 */

if(!String.prototype.slugify) {
    Object.defineProperty(String.prototype, "slugify", {
        enumerable: false,
        value: function() {
            return this.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
        }
    });
}