/**
 * Import server startup through a single index entry point
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import './fixtures.js';
import './register-api.js';
import './modules.js';