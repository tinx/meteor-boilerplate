/**
 * Modules listener
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { Modules } from '../../api/modules/modules.js';

Meteor.startup(() => {
    
    Meteor.setInterval(function() 
    {
        // read the modules file
        var modules = Assets.getText("modules.json");

        // get parse the json file
        var jsonModules = JSON.parse(modules);

        // init the module names
        var moduleNames = [];

        // loop
        jsonModules.forEach(function(jsonModule) 
        {
            // add to module names list
            moduleNames.push(jsonModule.name);

            // get this module based on name
            var m = Modules.findOne({ name: jsonModule.name });

            // when is undefined, insert in the database
            if(typeof m == "undefined") {
                console.log('Adding module ' + jsonModule.name + "...");
                Meteor.call('modules.save', jsonModule);
            }
        });

        // delete all the modules in backend that are not in the json file
        Modules.find({ name: { $nin: moduleNames }}).map(function(myModule) {
            console.log('Removing module ' + myModule.name + "...");
            Meteor.call('modules.remove', myModule._id);
        });

    }, 1000);

});
