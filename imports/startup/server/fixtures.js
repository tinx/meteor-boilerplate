
/**
 * Do something at startup (e.g. fill the DB with example data on startup)
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

import { Meteor } from 'meteor/meteor';
import { Settings } from '../../api/settings/settings.js';
import { Users } from '../../api/users/users.js';

Meteor.startup(() => {

    console.log('Application server is starting...');

    // add initial settings
    if(Settings.find({}).count() === 0) {

        console.log('Inserting default settings...');
        
        const settings = [
            {
                key: 'siteTitle',
                value: 'Default App',
            }
        ];

        settings.forEach(setting => Settings.insert(setting));
    }

    // add initial users
    if(Users.find({}).count() === 0) {

        console.log('Inserting default users...');

        const users = [
            {
                username: "admin",
                password: "admin",
                isGod: true,
                email: "admin@admin.com"
            },
            {
                username: "guest",
                password: "guest",
                isGod: false,
                email: "guest@guest.com"
            }
        ];

        users.forEach(user => Users.insert(user));
    }
    
});
