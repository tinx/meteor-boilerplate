/**
 * Register the apis
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */

/**
 * Settings
 */

import '../../api/settings/methods.js';
import '../../api/settings/server/publications.js';

/**
 * Users
 */

import '../../api/users/methods.js';
import '../../api/users/server/publications.js';

/**
 * Modules
 */

import '../../api/modules/methods.js';
import '../../api/modules/server/publications.js';