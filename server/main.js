/**
 * Server entry point, imports all server code
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com> 
 */

import '/imports/startup/server';
import '/imports/startup/both';