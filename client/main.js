/**
 * Client entry point, imports all client code
 * 
 * @author Tim De Paepe <tim.depaepe@gmail.com>
 */ 

// External libraries
import './lib/base/lib/admin-lte/dist/js/app.min.js';

// App libraries
import '/imports/startup/client';
import '/imports/startup/both';