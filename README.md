# Meteor Boilerplate

## Description

This boilerplate was created to develop Meteor 1.6 application with a Frontend and Backend.

## Installation

First run `meteor npm install`.  
Clone and start via `meteor -p [PORT]`.

## Backend Location

The Backend can be accessed via http://YOUR_URL:PORT/admin  
The Frontend can be accessed via http://YOUR_URL:PORT  

The default credentials for the backend are

- __user__: admin
- __password__: admin

## Version

### Version 0.1

- Added a configuration section with settings and user management
- Added a login for backend
- Added Modules